#!/bin/sh
# Battery :
bat(){
	for battery in /sys/class/power_supply/BAT?*; do
		[ -n "${capacity+x}" ] && printf " " # If non-first battery, print a space separator.
		case "$(cat "$battery/status" 2>&1)" in # Sets up the status and capacity
			"Full") status="" ;;
			"Discharging") status="" ;;
			"Charging") status="" ;;
			"Not charging") status="" ;;
			"Unknown") status="" ;;
			*) exit 1 ;;
		esac
		capacity="$(cat "$battery/capacity" 2>&1)"
		[ "$status" = "" ] && [ "$capacity" -le 25 ] && warn=" " # Will make a warn variable if discharging and low
		printf "%s%s%d%%" "$status" "$warn" "$capacity"; unset warn
	done && printf "\\n"
}

# Date && clock :
dat(){
	date="$(date +"%a %d %b %H:%M"| sed 's/  / /g')"
	echo -e " $date"
}

# Cpu Temp :
cputemp(){
	sensors | awk '/Core 0/ {print "" $3}'
}

# Cpu Usage :
cpu(){
	read cpu a b c previdle rest < /proc/stat
	prevtotal=$((a+b+c+previdle))
	sleep 0.5
	read cpu a b c idle rest < /proc/stat
	total=$((a+b+c+idle))
	cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
	echo -e " $cpu%"
}

# Keybord Layout :
key(){
	kb="$(xkb-switch)" || exit 1
	echo " $kb"
}

# Screen Light :
lit(){
	lit="$(brightnessctl | grep -oP '[^()]+%')"
	echo " $lit"	
}

# Memory :
mem(){
	mem="$(free -h | awk '/^Mem/ { print $3 }' | sed s/i//g)"
	echo -e " $mem" 
}

# Volume :
vol(){
	[ $(pamixer --get-mute) = true ] && echo 🔇 && exit
	vol="$(pamixer --get-volume)"
	if [ "$vol" -gt "70" ]; then
		icon=""
	elif [ "$vol" -gt "30" ]; then
		icon=""
	elif [ "$vol" -gt "0" ]; then
		icon="" 
	else
		echo  && exit
	fi
	echo "$icon $vol%"
}

# Network traffic
nettrf(){
	update() {
		sum=0
		for arg; do
			read -r i < "$arg"
			sum=$(( sum + i ))
		done
		cache=${XDG_CACHE_HOME:-$HOME/.cache}/${1##*/}
		[ -f "$cache" ] && read -r old < "$cache" || old=0
		printf %d\\n "$sum" > "$cache"
		printf %d\\n $(( sum - old ))
	}
	rx=$(update /sys/class/net/[ew]*/statistics/rx_bytes)
	tx=$(update /sys/class/net/[ew]*/statistics/tx_bytes)
	printf " %4sB  %4sB\\n" $(numfmt --to=iec $rx) $(numfmt --to=iec $tx)	
}

# Updates
pkg_updates() {
    updates=$(pacman --sync --sysupgrade --print 2> /dev/null | wc -l)
	#updates=$(xbps-install -un | wc -l) # void
	#updates=$(pacman -Qu | grep -Fcv "[ignored]" | sed "s/^//;s/^0$//g")   # arch , needs pacman contrib
	# updates=$(aptitude search '~U' | wc -l)  # apt (ubuntu,debian etc)
	printf " $updates"
}

while true; do
  xsetroot -name " $(pkg_updates) $(nettrf)  $(cpu)  $(cputemp)  $(mem)  $(vol)  $(dat)"
  sleep 0.2
done &
