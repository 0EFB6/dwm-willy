/* See LICENSE file for copyright and license details. */

/* appearance */

static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 16;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int user_bh            = 28;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */

static const char *fonts[]          = 
{
"JetBrains Mono:style=Bold:size=14:antialias=true:autohint=true",
"JetBrainsMono Nerd Font:style=Bold:size=12:antialias=true:autohint=true",
//"SauceCodePro Nerd Font Mono:weight=bold:pixelsize=10:antialias=true:hinting=true",
//"FontAwesome:size=8"
//"Monospace:size=15"
"JoyPixels:size=12:antialias=true:autohint=true"
};
static const char dmenufont[]       = "fontawesome:size=13";

/* Theme */
#include "themes/default.h"
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_3, col_1, col_2 },
	[SchemeSel]  = { col_4, col_5,  col_5  },
};

/* tagging */
// static const char *tags[] = { "", "", "", "", "", "", "", "", "", "", "", "12"};
// static const char *tags[] = {"", "", "", "", "", "", "", "", ""};
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
//static const char *tags[] = {"", "", "", "", "", "", "", "", ""};
//static const char *tags[] = {"", "", "", "","", "", "", "", "", "", ""};
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9"};
static const char *tags[] = { "", "", "", "", "", "", "", "", "", ""};

/* Underline Tags */
static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const char ptagf[] = "[%s %s]";	/* format of a tag label */
static const char etagf[] = "[%s]";	/* format of an empty tag */
static const int lcaselbl = 0;		/* 1 means make tag label lowercase */	

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      		instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     		NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  		NULL,       NULL,       1 << 1,       0,           -1 },
	{ "obs",      		NULL,	    NULL,	1 << 7,       0,	   -1 },
	{ "Alacritty",		NULL,	    NULL,	0,	      0,	   -1 },
	{ "discord",  		NULL,	    NULL,	1 << 3,	      0,           -1 },
	{ "Pcmanfm",  		NULL,	    NULL,	1 << 2,	      0,	   -1},
	{ "Chromium",		NULL,	    NULL,	1 << 0,	      0,           -1},
	{ "Spotify",		NULL,	    NULL,	1 << 4,	      0,           -1},
	{ "VeraCrypt",		NULL,	    NULL,	1 << 6,	      0,           -1},
	{ "TelegramDesktop",	NULL,	    NULL,	1 << 5,	      0,           -1},
	{ "thunderbird",	NULL,	    NULL,	1 << 5,	      0,           -1},

	// Floating
	{ "Xmessage", 		NULL,	  	NULL,	    0,            1,	       -1 },
	{ "Steam",    		NULL,       	NULL,       0,            1,           -1 },
	{ "Virt-manager",   	NULL,       	NULL,       0,            1,           -1 },
	{ "Pavucontrol",    	NULL,       	NULL,       0,            1,           -1 },
	{ "Qalculate-gtk",  	NULL,       	NULL,       0,            1,           -1 },
	{ "Lxappearance",   	NULL,       	NULL,       0,            1,           -1 },
	{ "KeePassXC",		NULL,		NULL,	    0,	      	  1,           -1}
};


/* layout(s) */
#include "layouts.c"
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[M]",      tile 			}, //tile
	{ "[N]",      NULL 			}, 
 	{ "HHH",        grid 			}, //grid
	{ "[]=",      monocle 		},  
	{ "TTT",      bstack 		}, //bottomstack
	{ "===",      bstackhoriz 	}, //bottomstack
};

/*
 *shift       Shift_L (0x32),  Shift_R (0x3e)
 *lock        Caps_Lock (0x42)
 *control     Control_L (0x25),  Control_R (0x6d)
 *mod1        Alt_L (0x40),  Alt_L (0x7d),  Meta_L (0x9c)
 *mod2        Num_Lock (0x4d)
 *mod3      
 *mod4        Super_L (0x7f),  Hyper_L (0x80)
 *mod5        Mode_switch (0x5d),  ISO_Level3_Shift (0x7c)
*/

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_1, "-nf", col_3, "-sb", col_5, "-sf", col_4, NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,						XK_z,	   focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_x,      focusstack,     {.i = -1 } },
	/* Movestack Patches*/
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	/*{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },*/
	/*{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },*/
	{ MODKEY,                       XK_j,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_k,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,           XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,						XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_n,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_comma,  setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_period, setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_slash,  setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_p,	   focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_i,	   focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_p,	   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,	   tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	/*{ ClkWinTitle,          0,              Button2,        zoom,           {0} },*/
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

